﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ProjectOrion.Camera
{
    public class OTexture
    {
        public Texture2D texture;
        public Vector2 size => texture.Bounds.Size.ToVector2();

        public OTexture(Texture2D texture)
        {
            this.texture = texture;
        }
    }
}