﻿using System;
using Microsoft.Xna.Framework;

namespace ProjectOrion.Camera
{
    public class Shaker
    {
        private float time;

        public void update()
        {
            time += Timing.elapsedTime;
        }

        public Vector2 getOffset()
        {
            return new Vector2(getAxis(time * 0.5f) * 2, getAxis(time));
        }

        public float getAxis(float t)
        {
            return (float) (0.5f * Math.Sin(t * 0.5f))
                   + (float) (0.5f * Math.Sin(t * 1.5f))
                   + (float) (0.2f * Math.Sin(t * 3f))
                   + (float) (0.15f * Math.Sin(t * 4f))
                   + (float) (0.1f * Math.Sin(t * 8f))
                   + (float) (0.1f * Math.Sin(t * 9.5f));
        }
    }
}