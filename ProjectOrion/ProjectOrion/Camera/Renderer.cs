﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ProjectOrion.World.Player;
using ProjectOrion.World.Ships;

namespace ProjectOrion.Camera
{
    public static class Renderer
    {
        private static ContentManager content;
        private static SpriteBatch spriteBatch;
        private static GraphicsDeviceManager graphics;
        private static GraphicsDevice graphicsDevice;

        private static RenderTarget2D renderTarget;

        private static Shaker shaker;

        public static float bluetime = 25;
        public static float transtime = 5;
        public static float redtime = 10;


        private static Vector2 targetOffset;

        public static Point canvasSize = new Point(960, 540);
        
        public static void init(ContentManager _content, SpriteBatch _spriteBatch, GraphicsDeviceManager _graphics)
        {
            content = _content;
            spriteBatch = _spriteBatch;
            graphics = _graphics;
            graphicsDevice = _graphics.GraphicsDevice;
            
            renderTarget = new RenderTarget2D(graphicsDevice, canvasSize.X, canvasSize.Y);
            Projector.adjustOrigin(canvasSize.ToVector2());
            
            shaker = new Shaker();
        }
        
        public static OTexture requireTexture(string path)
        {
            if (path == "%white%")
            {
                Color[] c = new Color[16 * 16];
                for (int i = 0; i < 16 * 16; i++)
                    c[i] = Color.White;
                Texture2D texture = new Texture2D(graphicsDevice, 16, 16);
                texture.SetData(c);
                return new OTexture(texture);
            }
            var tex = content.Load<Texture2D>(path);
            return new OTexture(tex);
        }

        public static float bgTimer;
        
        public static void startDraw()
        {
            bgTimer += Timing.elapsedTime;
            shaker.update();

            targetOffset = PlayerManager.players[0].position; // ShipManager.playerShip.position;
            Vector2 offsetD = targetOffset - Projector.worldTarget;
            if (offsetD.LengthSquared() > 0.1f)
            {
                Projector.worldTarget += offsetD * Timing.elapsedTime * 2;
            }
            
            graphicsDevice.SetRenderTarget(renderTarget);
            if (bgTimer < bluetime)
            {
                graphicsDevice.Clear(new Color(0.37f, 0.8f, 0.89f));
            } else if (bgTimer < transtime + bluetime)
            {
                graphicsDevice.Clear(new Color(0.37f+((bgTimer-bluetime)/transtime)*0.33f, 0.8f - ((bgTimer - bluetime) / transtime) * 0.5f, 0.89f - ((bgTimer - bluetime) / transtime) * 0.79f));
            }
            else
            {
                graphicsDevice.Clear(new Color(0.7f + (float)Math.Sin(bgTimer) * 0.12f, 0.3f + (float)Math.Sin(bgTimer * 1.3f) * 0.05f, 0.1f));
            }
            spriteBatch.Begin();
        }

        public static void draw(OTexture texture, Vector2 position, Vector2 size, float transparency = 1)
        {
            draw(texture, position, size, Color.White, transparency);
        }
        public static void draw(OTexture texture, Vector2 position, Vector2 size, Color c, float transparency = 1)
        {
            spriteBatch.Draw(texture.texture, Projector.toScreenPos(position, size), c * transparency);
        }
        public static void draw(OTexture texture, Vector2 position, Vector2 size, float rotation, Vector2 pivot, Color c, float transparency = 1)
        {
            spriteBatch.Draw(texture.texture, Projector.toScreenPos(position, size), null, c * transparency, rotation,
                             pivot * texture.size, SpriteEffects.None, 0);
        }

        public static void endDraw()
        {
            spriteBatch.End();
            graphicsDevice.SetRenderTarget(null);

            int width = graphics.PreferredBackBufferWidth;
            int height = graphics.PreferredBackBufferHeight;


            int texWidth = (int) (width * 2.5f);
            int texHeight = texWidth / 2;

            // For debugging purposes:
//            texWidth /= 4;
//            texHeight /= 4;

            Vector2 position = new Vector2(width / 2, height / 2);
            position += shaker.getOffset() * (width + height) * 0.015f;
            
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, null);
            spriteBatch.Draw(renderTarget, new Rectangle((int)position.X, (int)position.Y, texWidth, texHeight), null, Color.White,
                             MathHelper.Pi * -0.25f, canvasSize.ToVector2() / 2, SpriteEffects.None, 0);
            spriteBatch.End();
        }

        public static Vector2 toCanvasPos(Vector2 worldPos)
        {
            int width = graphics.PreferredBackBufferWidth;
            int height = graphics.PreferredBackBufferHeight;


            int texWidth = (int) (width * 3f);
            int texHeight = texWidth / 2;
            
            Vector2 centeredPos = worldPos - new Vector2(width / 2, height / 2);
            Vector2 rotPos = Vector2.Transform(centeredPos, Matrix.CreateRotationZ(MathHelper.Pi * 0.25f));

            rotPos /= width;

            rotPos *= canvasSize.X / 2;
            
            return rotPos + canvasSize.ToVector2() / 2;
        }
    }
}