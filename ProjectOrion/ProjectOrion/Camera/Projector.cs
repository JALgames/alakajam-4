﻿using Microsoft.Xna.Framework;

namespace ProjectOrion.Camera
{
    public static class Projector
    {
        public static Vector2 staticOffset;
        public static Vector2 offset => staticOffset + worldTarget;
        public static Vector2 roundedOffset => new Vector2((int) (offset.X * 16) / 16f, (int) (offset.Y * 16) / 16f);
        public static Vector2 worldTarget;
        public static float scale = 16;

        public static Rectangle toScreenPos(Vector2 position, Vector2 size)
        {
            return new Rectangle(((position - roundedOffset) * scale).ToPoint(), (size * scale).ToPoint());
        }
        public static Vector2 toWorldPos(Point screenPos)
        {
            Vector2 canvasPos = Renderer.toCanvasPos(screenPos.ToVector2());
            return (canvasPos / scale) + roundedOffset;
        }

        public static void adjustOrigin(Vector2 screenSize)
        {
            Vector2 worldPoint = screenSize / scale / 2;
            staticOffset = -worldPoint;
        }
    }
}