﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace ProjectOrion.Geometry
{
    public partial class AxisAlignedRectangle
    {
        public Vector2 position;
        public Vector2 size;

        public Vector2 topRight => position + new Vector2(size.X, 0);
        public Vector2 bottomLeft => position + new Vector2(0, size.Y);
        public Vector2 bottomRight => position + size;

        public Vector2 center => position + size * 0.5f;

        public float x => position.X;
        public float y => position.Y;
        public float width => size.X;
        public float height => size.Y;
        public float right => position.X + size.X;
        public float bottom => position.Y + size.Y;
        
        // Yes, these are exactly the same values as above, just with different names. It made sense in the other project where I copied these from. Now I'm just too lazy to refactor...
        public float minX => position.X;
        public float minY => position.Y;
        public float maxX => position.X + size.X;
        public float maxY => position.Y + size.Y;

        public AxisAlignedRectangle(Vector2 position, Vector2 size)
        {
            this.position = position;
            this.size = size;
        }
        public AxisAlignedRectangle(float x, float y, float width, float height)
        {
            this.position = new Vector2(x, y);
            this.size = new Vector2(width, height);
        }

        public float getDistance(Vector2 point)
        {
            float closestX = point.X;
            if (point.X < minX)
                closestX = minX;
            else if (point.X > maxX)
                closestX = maxX;
            float closestY = point.Y;
            if (point.Y < minY)
                closestY = minY;
            else if (point.Y > maxY)
                closestY = maxY;
            return (new Vector2(closestX, closestY) - point).Length();
        }

        public static AxisAlignedRectangle operator *(AxisAlignedRectangle a, float s)
        {
            return new AxisAlignedRectangle(a.position * s, a.size * s);
        }
        public static AxisAlignedRectangle operator *(float s, AxisAlignedRectangle a)
        {
            return new AxisAlignedRectangle(a.position * s, a.size * s);
        }

        public override string ToString()
        {
            return "AxisAlignedRectangle: x=" + x + "; y=" + y + "; width=" + width + "; height=" + height;
        }
    }
}