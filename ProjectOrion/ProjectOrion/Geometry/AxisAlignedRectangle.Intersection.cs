﻿using Microsoft.Xna.Framework;

namespace ProjectOrion.Geometry
{
    public partial class AxisAlignedRectangle
    {
        public bool containsPoint(Vector2 point)
        {
            return point.X >= position.X
                   && point.Y >= position.Y
                   && point.X <= position.X + size.X
                   && point.Y <= position.Y + size.Y;
        }
        
        public bool intersectsAxisAlignedRectangle(AxisAlignedRectangle o)
        {
            float x1 = position.X;
            float x2 = x1 + size.X;
            float x3 = o.position.X;
            float x4 = x3 + o.size.X;
            if ((!(x1 >= x3) || !(x1 <= x4)) && (!(x3 >= x1) || !(x3 <= x2)))
                return false;
            
            float y1 = position.Y;
            float y2 = y1 + size.Y;
            float y3 = o.position.Y;
            float y4 = y3 + o.size.Y;
            return (y1 >= y3 && y1 <= y4) || (y3 >= y1 && y3 <= y2);
        }
    }
}