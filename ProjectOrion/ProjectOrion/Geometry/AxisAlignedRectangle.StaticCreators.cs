﻿using Microsoft.Xna.Framework;

namespace ProjectOrion.Geometry
{
    public partial class AxisAlignedRectangle
    {
        public static ProjectOrion.Geometry.AxisAlignedRectangle fromMinMax(float xMin, float yMin, float xMax, float yMax)
        {
            return new ProjectOrion.Geometry.AxisAlignedRectangle(xMin, yMin, xMax - xMin, yMax - yMin);
        }
        public static ProjectOrion.Geometry.AxisAlignedRectangle fromMinMax(Vector2 min, Vector2 max)
        {
            return new ProjectOrion.Geometry.AxisAlignedRectangle(min.X, min.Y, max.X - min.X, max.Y - min.Y);
        }

        public static ProjectOrion.Geometry.AxisAlignedRectangle fromSize(float size)
        {
            return new ProjectOrion.Geometry.AxisAlignedRectangle(new Vector2(size / -2), new Vector2(size));
        }
        public static ProjectOrion.Geometry.AxisAlignedRectangle fromSize(float x, float y)
        {
            return new ProjectOrion.Geometry.AxisAlignedRectangle(new Vector2(x / -2, y / -2), new Vector2(x, y));
        }
        public static ProjectOrion.Geometry.AxisAlignedRectangle fromSize(Vector2 size)
        {
            return new ProjectOrion.Geometry.AxisAlignedRectangle(-(size / 2), size);
        }
        public static ProjectOrion.Geometry.AxisAlignedRectangle fromCenterSize(Vector2 center, float size)
        {
            return new ProjectOrion.Geometry.AxisAlignedRectangle(center - new Vector2(size / 2), new Vector2(size));
        }
        public static ProjectOrion.Geometry.AxisAlignedRectangle fromCenterSize(Vector2 center, float x, float y)
        {
            return new ProjectOrion.Geometry.AxisAlignedRectangle(center - new Vector2(x / 2, y / 2), new Vector2(x, y));
        }
        public static ProjectOrion.Geometry.AxisAlignedRectangle fromCenterSize(Vector2 center, Vector2 size)
        {
            return new ProjectOrion.Geometry.AxisAlignedRectangle(center - size / 2, size);
        }
    }
}