﻿using System.Diagnostics.Contracts;
using Microsoft.Xna.Framework;

namespace ProjectOrion.Geometry
{
    public partial class AxisAlignedRectangle
    {
        [Pure]
        public ProjectOrion.Geometry.AxisAlignedRectangle translateAxisAlignedRectangle(Vector2 d)
        {
            return new ProjectOrion.Geometry.AxisAlignedRectangle(position + d, size);
        }
        [Pure]
        public ProjectOrion.Geometry.AxisAlignedRectangle translateAxisAlignedRectangle(float x, float y)
        {
            return new ProjectOrion.Geometry.AxisAlignedRectangle(new Vector2(position.X + x, position.Y + y), size);
        }
        
        [Pure]
        public ProjectOrion.Geometry.AxisAlignedRectangle scaleAxisAlignedRectangle(float scale)
        {
            return new ProjectOrion.Geometry.AxisAlignedRectangle(position + size * 0.5f - size * scale * 0.5f, size * scale);
        }
        [Pure]
        public ProjectOrion.Geometry.AxisAlignedRectangle scaleAxisAlignedRectangle(Vector2 scale)
        {
            return new ProjectOrion.Geometry.AxisAlignedRectangle(position + size * 0.5f - size * scale * 0.5f, size * scale);
        }

        [Pure]
        public ProjectOrion.Geometry.AxisAlignedRectangle addBorderAxisAlignedRectangle(float size)
        {
            return new ProjectOrion.Geometry.AxisAlignedRectangle(x - size, y - size, width + 2 * size, height + 2 * size);
        }
    }
}