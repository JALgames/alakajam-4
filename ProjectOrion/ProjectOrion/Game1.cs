﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ProjectOrion.Camera;
using ProjectOrion.Overlays;
using ProjectOrion.World;

namespace ProjectOrion
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "content";
            this.Window.AllowUserResizing = true;
            this.Window.ClientSizeChanged += Window_ClientSizeChanged;

        }

        void Window_ClientSizeChanged(object sender, EventArgs e)
        {
            if (graphics.PreferredBackBufferWidth != Window.ClientBounds.Width
                || graphics.PreferredBackBufferHeight != Window.ClientBounds.Height
                && Window.ClientBounds.Width != 0
                && Window.ClientBounds.Height != 0)
            {
                graphics.PreferredBackBufferWidth = Window.ClientBounds.Width;
                graphics.PreferredBackBufferHeight = Window.ClientBounds.Height;
                graphics.ApplyChanges();
            }
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Renderer.init(Content, spriteBatch, graphics);
            OverlayManager.init();
            
            WorldManager.init();
        }
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            Timing.setTime((float) gameTime.ElapsedGameTime.TotalSeconds);
            Input.update();
            WorldManager.update();
            
            
            base.Update(gameTime);
        }
        protected override void Draw(GameTime gameTime)
        {
            Renderer.startDraw();
            
            WorldManager.draw();
            OverlayManager.draw();
            
            Renderer.endDraw();
            base.Draw(gameTime);
        }
    }
}
