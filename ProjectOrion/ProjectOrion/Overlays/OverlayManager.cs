﻿using Microsoft.Xna.Framework;
using ProjectOrion.Camera;

namespace ProjectOrion.Overlays
{
    public static class OverlayManager
    {
        public static OTexture background;
        public static OTexture currentOverlay;

        public static float currentTime;
        public static float totalTime;
        
        public static void init()
        {
            background = Renderer.requireTexture("%white%");
        }

        public static void draw()
        {
            if (currentOverlay == null)
                return;
            currentTime += Timing.elapsedTime;

            if (currentTime >= totalTime)
            {
                currentOverlay = null;
                return;
            }

            float opacity = 1;
            float fadeSpeed = 0.5f;
            if (currentTime < fadeSpeed)
            {
                opacity = currentTime / fadeSpeed;
            }
            else if (currentTime > totalTime - fadeSpeed)
            {
                opacity = (totalTime - currentTime) / fadeSpeed;
            }

            Vector2 center = Projector.worldTarget;

            Vector2 bgSize = new Vector2(30, 30);
            Renderer.draw(background,  center - bgSize / 2, bgSize, Color.Black, opacity * 0.5f);

            Vector2 overlaySize = currentOverlay.size / Projector.scale; // Let's not draw a texture because we never needed that particular feature
//            Renderer.draw(currentOverlay, center - overlaySize / 2, overlaySize, Color.White, opacity);
        }

        public static void showOverlay(OTexture texture, float time)
        {
            currentOverlay = texture;
            totalTime = time;
            currentTime = 0;
        }
    }
}