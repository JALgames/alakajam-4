﻿using Microsoft.Xna.Framework.Input;

namespace ProjectOrion
{
    public static class Input
    {
        public static KeyboardState lastKeyboard;
        public static KeyboardState keyboard;

        public static MouseState mouse;
        
        public static void update()
        {
            lastKeyboard = keyboard;
            keyboard = Keyboard.GetState();

            mouse = Mouse.GetState();

        }

        public static bool startedPress(Keys key)
        {
            return !lastKeyboard.IsKeyDown(key) && keyboard.IsKeyDown(key);
        }
    }
}