﻿using System;
using Microsoft.Xna.Framework;
using ProjectOrion.World.Player.AI;
using ProjectOrion.World.Ships;

namespace ProjectOrion.World
{
    public class GameDirector
    {
        public static float timer = 19;
        public static void update()
        {
            float oldTimer = timer;
            timer += Timing.elapsedTime;

            if (timer > 32)
            {
                if (ShipManager.ships.Count == 1)
                    AttackFromBehindAI.create();
                
            }
            if ((timer > 55 && ShipManager.helibuddies.Count < 1) || (timer > 80 && ShipManager.helibuddies.Count < 2))
            {
                Random rnd = new Random();
                Vector2 position = new Vector2(-7, rnd.NextFloat(2, 3) * (rnd.Next(0, 2) - 1 * 2));
                Heli heli = new Heli(new Vector2(rnd.NextFloat(15, -5), rnd.NextFloat(3, 5) * (rnd.Next(0, 2) - 1 * 2)), position);
                ShipManager.helibuddies.Add(heli);
            }
        }
    }
}