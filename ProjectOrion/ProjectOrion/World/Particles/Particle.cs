﻿using Microsoft.Xna.Framework;
using ProjectOrion.Camera;

namespace ProjectOrion.World.Particles
{
    public class Particle
    {
        public int texture;
        
        public Color color;
        public Vector2 position;
        public Vector2 direction;
        public Vector2 acceleration;
        public Vector2 size;

        public float rotation;
        public float rotationVelocity;
        
        public float lifetime;

        public float transparency;

        public Particle(Color color, Vector2 position, Vector2 direction, Vector2 size, float lifetime)
        {
            this.color = color;
            this.position = position;
            this.direction = direction;
            this.size = size;
            this.lifetime = lifetime;

            this.transparency = 1;
        }

        public void update()
        {
            direction += acceleration * Timing.elapsedTime;
            position += direction * Timing.elapsedTime;
            lifetime -= Timing.elapsedTime;
            rotation += rotationVelocity * Timing.elapsedTime;
        }

        public void draw()
        {
            if (lifetime <= 0)
                return;
            OTexture texture = ParticleSystem.textures[this.texture];
            Renderer.draw(texture, position - size / 2, size, rotation, new Vector2(0.5f, 0.5f), color, transparency);
        }
    }
}