﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using ProjectOrion.Camera;

namespace ProjectOrion.World.Particles
{
    public static class ParticleSystem
    {
        public static List<OTexture> textures;
        
        public static List<Particle> particles;
        public static float lifetimeCheck;

        private static Random rnd;
        
        public static void init()
        {
            particles = new List<Particle>();
            textures = new List<OTexture>();
            textures.Add(Renderer.requireTexture("%white%"));
            rnd = new Random();
        }

        
        public static void update()
        {
            foreach (var particle in particles)
                particle.update();

            lifetimeCheck += Timing.elapsedTime;
            if (lifetimeCheck > 0.5f)
            {
                checkTime();
                lifetimeCheck = 0;
            }
        }

        private static void checkTime()
        {
            for (int i = particles.Count - 1; i >= 0; i--)
            {
                if (particles[i].lifetime < 0)
                    particles.RemoveAt(i);
            }
        }

        public static void draw()
        {
            foreach (var particle in particles)
                particle.draw();
        }

        public static void shieldEffect(Vector2 position, float mainDirection)
        {
            int count = rnd.Next(5, 8);
            for (int i = 0; i < count; i++)
            {
                float angle = rnd.NextFloat(mainDirection - MathHelper.PiOver2, mainDirection + MathHelper.PiOver2);
                Vector2 d = new Vector2((float) Math.Cos(angle), (float) Math.Sin(angle));
                float speed = rnd.NextFloat(3, 5);
                
                float lifetime = rnd.NextFloat(0.2f, 0.4f);

                float blueChannel = rnd.NextFloat(0.8f, 0.95f);
                float otherChannels = rnd.NextFloat(0.2f, 0.4f);

                Particle p = new Particle(new Color(otherChannels, (otherChannels + blueChannel) / 2, blueChannel), position, d * speed, new Vector2(0.1f, 0.5f), lifetime);
                p.rotation = angle + MathHelper.PiOver2;
                particles.Add(p);
            }
        }
        public static void explosion(Vector2 position, float mainDirection)
        {
            int count = rnd.Next(15, 20);
            for (int i = 0; i < count; i++)
            {
                float angle = rnd.NextFloat(mainDirection - MathHelper.PiOver2, mainDirection + MathHelper.PiOver2);
                Vector2 d = new Vector2((float) Math.Cos(angle), (float) Math.Sin(angle));
                float speed = rnd.NextFloat(1, 1.5f);
                
                float lifetime = rnd.NextFloat(0.2f, 0.4f);

                float redChannel = rnd.NextFloat(0.8f, 0.95f);
                float otherChannels = rnd.NextFloat(0.2f, 0.4f);

                Particle p = new Particle(new Color(redChannel, otherChannels, otherChannels), position, d * speed, new Vector2(0.1f, 0.5f), lifetime);
                p.rotation = angle;
                particles.Add(p);
            }
        }
        public static void gore(Vector2 position)
        {
            int count = rnd.Next(50, 100);
            for (int i = 0; i < count; i++)
            {
                float angle = rnd.NextFloat( MathHelper.Pi * 1.3f, MathHelper.Pi * 1.7f);
                Vector2 d = new Vector2((float) Math.Cos(angle), (float) Math.Sin(angle));
                float speed = rnd.NextFloat(7, 16);
                
                float lifetime = rnd.NextFloat(0.3f, 0.7f);

                float redChannel = rnd.NextFloat(0.5f, 0.95f);
                float otherChannels = rnd.NextFloat(0.0f, 0.1f);

                Particle p = new Particle(new Color(redChannel, otherChannels, otherChannels), position, d * speed, new Vector2(0.2f, 0.2f), lifetime);
                p.rotation = rnd.NextFloat(0, 6.28f);
                p.acceleration = new Vector2(0, 25);
                particles.Add(p);
            }
        }
    }
}