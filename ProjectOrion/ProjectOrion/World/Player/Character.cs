﻿using System;
using System.Runtime.Remoting.Contexts;
using Microsoft.Xna.Framework;
using ProjectOrion.Camera;

namespace ProjectOrion.World.Player
{
    public class Character
    {
        public Vector2 position;

        public Vector2 lastPosition;
        public int lastDirection; // left = 0; right = 1;
        public float timeSinceTurn;
        public int playerColor;
       //public Color

        public OTexture textureL;
        public OTexture textureR;
        public OTexture textureF;
        public Character(int playerColor)
        {
            this.playerColor = playerColor;
            if (playerColor == 0)
            {
                textureL = Renderer.requireTexture("player/spaceBob");
                textureR = Renderer.requireTexture("player/spaceBobR");
                textureF = Renderer.requireTexture("player/spaceBobF");
            } else
            {
                textureL = Renderer.requireTexture("player/spaceBlob");
                textureR = Renderer.requireTexture("player/spaceBlobR");
                textureF = Renderer.requireTexture("player/spaceBlobF");
            }
        }

        public virtual void update()
        {
            
        }
        
        public virtual void draw()
        {
            if (timeSinceTurn < 0.1)
            {
                Renderer.draw(textureF, position, new Vector2(1, 1));
                if (position.X > lastPosition.X)
                {
                    lastDirection = 1;
                } else
                {
                    lastDirection = 0;
                }
                timeSinceTurn+=Timing.elapsedTime;
            }
            else
            {
                if (position.X > lastPosition.X)
                {
                    Renderer.draw(textureR, position, new Vector2(1, 1));
                    if (lastDirection == 0)
                    {
                        timeSinceTurn = 0;
                    }
                    lastDirection = 1;
                }
                else
                {
                    Renderer.draw(textureL, position, new Vector2(1, 1));
                    if (lastDirection == 1)
                    {
                        timeSinceTurn = 0;
                    }
                    lastDirection = 0;
                }
            }

            lastPosition = position;
        }

        public virtual bool collides()
        {
            return false;
        }
    }
}