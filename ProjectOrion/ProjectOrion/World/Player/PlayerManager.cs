﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using ProjectOrion.Camera;
using ProjectOrion.Overlays;
using ProjectOrion.World.Particles;
using ProjectOrion.World.Ships;

namespace ProjectOrion.World.Player
{
    public static class PlayerManager
    {
        public static List<Character> players;
        private static bool stillAlive = true;
        private static OTexture gameOver;

        public static void init()
        {
            players = new List<Character>();
            players.Add(new PlayerCharacter(0));
            stillAlive = true;

            gameOver = Renderer.requireTexture("environment/gameOver");
        }

        public static void update()
        {
            Vector2 playerPos = players[0].position;
            if ((playerPos - ShipManager.playerShip.position).Y > 50)
                killPlayer();
            
            foreach (var player in players)
                player.update();
            
            for (int i = players.Count - 1; i >= 1; i--)
            {
                if ((players[i].position - players[0].position).Length() < 1.5f)
                    killEnemy(players[i]);
            }
        }

        public static void draw()
        {
            foreach (var player in players)
                player.draw();
            if (stillAlive==false)
                Renderer.draw(gameOver, Projector.worldTarget - 0.4f * gameOver.size / Projector.scale, 0.8f*gameOver.size / Projector.scale);

        }

        public static void killPlayer()
        {
            OverlayManager.showOverlay(Renderer.requireTexture("%white%"), 4);
            players[0].position = ShipManager.playerShip.position + new Vector2(0.5f, 0.5f);
            ShipManager.playerLives--;
            if (ShipManager.playerLives == -1 || !ShipManager.ships.Contains(ShipManager.playerShip))
            {
                stillAlive = false;
                Timing.speedFactor = 0.1f;
            }
        }

        public static void killEnemy(Character enemy)
        {
            ParticleSystem.gore(enemy.position + new Vector2(0.5f, 0.5f));
            players.Remove(enemy);
        }
    }
}