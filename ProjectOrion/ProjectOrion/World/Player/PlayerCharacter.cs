﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using ProjectOrion.Camera;
using ProjectOrion.Geometry;
using ProjectOrion.World.Ships;
using ProjectOrion.World.Ships.Interactions;

namespace ProjectOrion.World.Player
{
    public class PlayerCharacter : Character
    {
        public Vector2 velocity;
        private bool canJump;

        public Interaction interaction;
        public Ship interactionShip;

        public Ship lastShip;
        
        public PlayerCharacter(int playerColor) : base(playerColor)
        {
            position = new Vector2(0, -2);
            interaction = null;
        }

        public override void update()
        {
            if (lastShip != null)
                ShipManager.playerShip = lastShip;
            base.update();
            bool ladders =
                ShipManager.ladders(
                    new AxisAlignedRectangle(position + new Vector2(0.15f, 0.25f), new Vector2(0.7f, 0.75f)));

                
            if (interaction == null)
            {
                float d = 0;
                float speed = 5;
                float acceleration = 20;
                float deceleration = 30;
                if (Input.keyboard.IsKeyDown(Keys.A))
                    d = -1;
                if (Input.keyboard.IsKeyDown(Keys.D))
                    d += 1;

                if (d != 0)
                    velocity.X += d * acceleration * Timing.elapsedTime;
                else
                {
                    float target = -(1 / 16f) * 15f; // Should ensure that we have one pixel movement every four frames
                    int velSign = Math.Sign(velocity.X - target);
                    velocity.X += -velSign * deceleration * Timing.elapsedTime;
                    if (velSign != Math.Sign(velocity.X - target))
                        velocity.X = target;
                }


                velocity.X = MathHelper.Clamp(velocity.X, -speed, speed);

                position.X += velocity.X * Timing.elapsedTime;
                if (collides(position))
                {
                    position.X -= velocity.X * Timing.elapsedTime;
                    velocity.X *= -0.7f;
                }

                if (ladders)
                {
                    if (Input.keyboard.IsKeyDown(Keys.W))
                        velocity.Y = -4;
                    else if (Input.keyboard.IsKeyDown(Keys.S))
                        velocity.Y = 4;
                    else
                    {
                        velocity.Y = 2;
                    }
                }
                else
                {
                    if (Input.keyboard.IsKeyDown(Keys.W) && canJump)
                    {
                        velocity.Y = -7;
                    }
                }

                if (Input.startedPress(Keys.Space))
                {
                    foreach (var ship in ShipManager.ships)
                    {
                        interaction = ship.interactions.join(position + new Vector2(0.5f, 0.5f));
                        if (interaction != null)
                        {
                            interactionShip = ship;
                            break;
                        }
                    }
                        
                }
            }
            else
            {
                interaction.update(this, interactionShip);
                if (Input.startedPress(Keys.Space))
                    interaction = null;
            }

            if (!ladders)
                velocity.Y += 15 * Timing.elapsedTime;

            canJump = false;
            if (ladders)
                canJump = true;
            position.Y += velocity.Y * Timing.elapsedTime;
            if (collides(position))
            {
                position.Y -= velocity.Y * Timing.elapsedTime;
                if (!ladders)
                {
                    if (velocity.Y > 0)
                    {
                        velocity.Y = 0;
                        canJump = true;
                    }
                    else
                        velocity.Y *= -0.5f;
                }
            }
            
            
        }

        public override bool collides()
        {
            return collides(position);
        }

        public bool collides(Vector2 position)
        {
            AxisAlignedRectangle collider = new AxisAlignedRectangle(position + new Vector2(0.15f, 0.25f), new Vector2(0.7f, 0.75f));

            Ship collidingShip;
            
            bool collision = ShipManager.collides(collider, out collidingShip);
            if (collidingShip != null)
                lastShip = collidingShip;
            return collision;
        }

        public override void draw()
        {
            base.draw();
            if (interaction != null)
                interaction.draw(this, interactionShip);
        }
    }
}