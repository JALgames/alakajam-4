﻿using System;
using System.Collections;
using Microsoft.Xna.Framework;
using ProjectOrion.World.Player.AI;
using ProjectOrion.World.Ships;

namespace ProjectOrion.World.Player
{
    public class EnemyCharacter : Character
    {
        public int level;

        public Vector2 localPosition;
        public Ship ship;

        public EnemyAI ai;
        
        public enum Locations
        {
            Steering,
            Weapons,
            Shields,
        }

        public Locations currentTarget;
        public bool atTarget;
        
        public EnemyCharacter(Ship ship, int playerColor) : base (playerColor)
        {
            currentTarget = Locations.Steering;
            localPosition = new Vector2(getStandingHeight(level), localPosition.X);

            this.ship = ship;
            
            this.ai = new EnemyAI();
        }

        public override void update()
        {
            float speed = 6;
            
            base.update();
            ai.update(this);
            
            position = localPosition + ship.position;

            if (level != getLevel(currentTarget))
            {
                if (localPosition.X < 1.5f)
                    localPosition.X += speed * Timing.elapsedTime;
                else
                {
                    float targetHeight = getStandingHeight(getLevel(currentTarget));

                    int d = Math.Sign(targetHeight - localPosition.Y);

                    localPosition.Y += d * speed * 0.7f * Timing.elapsedTime;
                    if (d != Math.Sign(targetHeight - localPosition.Y))
                    {
                        localPosition.Y = targetHeight;
                        level = getLevel(currentTarget);
                    }
                }
            }
            else
            {
                float targetPos = getPosition(currentTarget);
                int d = Math.Sign(targetPos - localPosition.X);
                localPosition.X += d * speed * Timing.elapsedTime;
                if (d == 0 || d != Math.Sign(targetPos - localPosition.X))
                {
                    localPosition.X = targetPos;
                    atTarget = true;
                }
            }
        }

        public bool goTo(Locations location)
        {
            if (currentTarget == location)
                return atTarget;
            currentTarget = location;
            atTarget = false;
            return false;
        }

        public void steer(Vector2 direction)
        {
            if (direction.Length() > 1)
                direction.Normalize();
            ship.move(direction * 6 * Timing.elapsedTime);
        }
        

        public static int getLevel(Locations location)
        {
            switch (location)
            {
                case Locations.Steering: return 2;
                case Locations.Weapons: return 1;
                case Locations.Shields: return 0;
            }
            return 0;
        }
        public static float getPosition(Locations location)
        {
            switch (location)
            {
                case Locations.Steering: return -0.5f;
                case Locations.Weapons: return -2.4f;
                case Locations.Shields: return 0.3f;
            }
            return 0;
        }

        public static float getStandingHeight(int level)
        {
            return getLevelHeight(level) - 1f;
        }

        public static float getLevelHeight(int level)
        {
            switch (level)
            {
                case 0: return 1.6f;
                case 1: return 0.43f;
                case 2: return -0.71f;
            }
            return 0;
        }
    }
}