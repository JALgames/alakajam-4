﻿
using System;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using ProjectOrion.World.Ships;
using ProjectOrion.World.Ships.Interactions;

namespace ProjectOrion.World.Player.AI
{
    public class EnemyAI
    {
        private int lastFloor;
        private float lastX;

        private EnemyActivities activity;

        
        protected float shieldAngle;
        protected Vector2 shieldPos;

        protected Vector2 playerShipPos;
        protected Vector2 thisShipPos;
        
        public EnemyAI()
        {
            
        }
        
        public virtual void update(EnemyCharacter enemy)
        {
            var newActivity = determineActivity();
            if (newActivity != EnemyActivities.Unknown)
                activity = newActivity;
            
            shieldAngle = ShipManager.playerShip.shieldPos;
            shieldPos = new Vector2((float) Math.Cos(shieldAngle), (float) Math.Sin(shieldAngle));

            playerShipPos = ShipManager.playerShip.position;
            thisShipPos = enemy.ship.position;

        }

        public EnemyActivities determineActivity()
        {
            PlayerCharacter character = (PlayerCharacter)PlayerManager.players[0];

            if (character.lastShip != ShipManager.playerShip)
            {
                return EnemyActivities.Outside;
            }

            if (character.interaction != null)
            {
                if (character.interaction.GetType() == typeof(ShieldPositioningInteraction))
                    return EnemyActivities.Shielding;
                if (character.interaction.GetType() == typeof(SteeringWheel))
                    return EnemyActivities.Steering;
                if (character.interaction.GetType() == typeof(ShootInteraction))
                    return EnemyActivities.Shooting;
                throw new Exception("Unknown player activity");
            }


            float positionX = character.position.X - character.lastShip.position.X;
            float positionY = character.position.Y - character.lastShip.position.Y;
            float[] distances = new float[3];
            for (int i = 0; i < 3; i++)
                distances[i] = Math.Abs(positionY - EnemyCharacter.getStandingHeight(i));

            int floor = lastFloor;
            for (int i = 0; i < 3; i++)
                if (distances[i] < 0.5f)
                    floor = i;

            lastFloor = floor;

            float dX = positionX - lastX;
            if (lastX != positionX)
                lastX = positionX;

            Debug.WriteLine(dX);
            
            switch (lastFloor)
            {
                case 0:
                    if (dX < 0)
                        return EnemyActivities.FromShielding;
                    else
                        return EnemyActivities.ToShielding;
                case 1:
                    if (dX < 0)
                        return EnemyActivities.FromShielding;
                    else
                        return EnemyActivities.ToShooting;
                case 2:
                    if (dX < 0)
                        return EnemyActivities.FromSteering;
                    else
                        return EnemyActivities.ToSteering;
            }


            return EnemyActivities.Unknown;
        }
        
        public enum EnemyActivities
        {
            Steering,
            FromSteering,
            ToSteering,
            
            Shooting,
            FromShooting,
            ToShooting,
            
            Shielding,
            FromShielding,
            ToShielding,
            
            Outside,
            Unknown
        }
    }
}