﻿using System;
using System.Linq;
using Microsoft.Xna.Framework;
using ProjectOrion.Camera;
using ProjectOrion.Overlays;
using ProjectOrion.World.Ships;
using ProjectOrion.World.Ships.Interactions;

namespace ProjectOrion.World.Player.AI
{
    public class AttackFromBehindAI : EnemyAI
    {
        public static Enemy create()
        {
            Enemy ship = new Enemy();
            ship.position = ShipManager.playerShip.position + new Vector2(30, 0);
            ship.shieldPos = MathHelper.Pi * 1.3f;
            
            ShipManager.ships.Add(ship);
            
            EnemyCharacter character = new EnemyCharacter(ship, 1);
            character.ai = new AttackFromBehindAI(ship);

            PlayerManager.players.Add(character);
            
            return ship;
        }


        enum States
        {
            MovingIn,
            Attacking,
            Shooting,
            BoostingForward
            
        }
        private States state = States.MovingIn;
        private Enemy ship;

        private float timer;
        private float targetHeight => (float) Math.Sin(timer) * 5 + 3f;
        

        public AttackFromBehindAI(Enemy ship)
        {
            this.ship = ship;
        }
        
        bool shouldBoost = new Random().Next(0, 3) == 0;
        
        public override void update(EnemyCharacter enemy)
        {
            ship.boost = false;
            timer += Timing.elapsedTime;
            base.update(enemy);
            if (state == States.MovingIn)
            {
                ship.boost = true;
                if (enemy.goTo(EnemyCharacter.Locations.Steering))
                {
                    enemy.steer(new Vector2(-1, 0));
                }
                if ((playerShipPos - thisShipPos).Length() < 8)
                    setState(States.Attacking);
            }
            else if (state == States.Attacking)
            {
                float dY = thisShipPos.Y + targetHeight - playerShipPos.Y;
                if (enemy.goTo(EnemyCharacter.Locations.Steering))
                {
                    enemy.steer(new Vector2(0, -dY / 2));
                }

                
                if (shouldBoost)
                {
                    if (targetHeight > 6.5f  && timer > 7)
                        setState( States.BoostingForward);
                }
                else
                {
                    if (targetHeight > 3 && targetHeight < 4 && timer > 15)
                        setState( States.Shooting);
                }
                
            }
            else if (state == States.Shooting)
            {
                timer = 0;
                if (enemy.goTo(EnemyCharacter.Locations.Weapons))
                {
                    Vector2 target = PlayerManager.players[0].position +
                                       (PlayerManager.players[0] as PlayerCharacter).velocity + new Vector2(0.5f, 0.5f);
                    foreach (var gun in enemy.ship.guns)
                        gun.aim(target, ship.position);
                    
                    foreach (var gun in enemy.ship.guns)
                        gun.fire(ship.position);

               
                }
                if (timer >= 10)
                {
                    
                    setState(States.Attacking);
                }
            }
            else if (state == States.BoostingForward)
            {
                if (enemy.goTo(EnemyCharacter.Locations.Steering))
                {
                    ship.boost = true;
                    enemy.steer(new Vector2(-0.7f, 0));
                    if (enemy.position.X <= playerShipPos.X - 3)
                    {
                        setState(States.Shooting);
                    }
                }
            }
        }

        private void setState(States state)
        {
            this.state = state;
            timer = 0;
        }
    }
}