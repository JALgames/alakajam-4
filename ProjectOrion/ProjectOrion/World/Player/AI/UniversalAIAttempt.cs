﻿using System;
using Microsoft.Xna.Framework;
using ProjectOrion.World.Ships;

namespace ProjectOrion.World.Player.AI
{
    public class UniversalAIAttempt : EnemyAI
    {
        
        enum Modes
        {
            Defensive,
            Offensive,
            Suicidal
        }

        private Modes mode;
        private float modeTimer;
        
        
        public override void update(EnemyCharacter enemy)
        {
            base.update(enemy);
            modeTimer += Timing.elapsedTime;
            if (modeTimer > 20)
            {
                int newMode = new Random().Next(0, 20);
                if (newMode < 10)
                    mode = Modes.Defensive;
                else if (newMode < 18)
                    mode = Modes.Offensive;
                else
                    mode = Modes.Suicidal;
            }
            
            
            float shieldAngle = ShipManager.playerShip.shieldPos;
            Vector2 shieldPos = new Vector2((float) Math.Cos(shieldAngle), (float) Math.Sin(shieldAngle));

            Vector2 playerShipPos = ShipManager.playerShip.position;
            Vector2 thisShipPos = enemy.ship.position;

            bool canJump = playerShipPos.Y <= thisShipPos.Y + 1
                           && Math.Abs(playerShipPos.X - thisShipPos.X) < 7 + 0.5f * (playerShipPos.Y - thisShipPos.Y);

            if (canJump)
            {
                if (enemy.goTo(EnemyCharacter.Locations.Steering))
                {
                    enemy.steer(thisShipPos - playerShipPos);
                }
            }
            else
            {
                enemy.goTo(EnemyCharacter.Locations.Weapons);
            }
        }
    }
}