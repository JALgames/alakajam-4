﻿using ProjectOrion.World.Environment;
using ProjectOrion.World.Particles;
using ProjectOrion.World.Player;
using ProjectOrion.World.Ships;

namespace ProjectOrion.World
{
    public class WorldManager
    {
        public static void init()
        {
            ParticleSystem.init();
            PlayerManager.init();
            ShipManager.init();
            CloudManager.init();
        }

        public static void update()
        {
            GameDirector.update();
            ShipManager.update();
            PlayerManager.update();
            CloudManager.update();
            ParticleSystem.update();
        }

        public static void draw()
        {
            CloudManager.draw(0);
            ShipManager.draw();
            PlayerManager.draw();
            ParticleSystem.draw();
            CloudManager.draw(1);
        }
    }
}