﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using Microsoft.Xna.Framework;
using ProjectOrion.Camera;
using ProjectOrion.World.Particles;
using ProjectOrion.World.Ships;

namespace ProjectOrion.World.Environment
{
    public static class CloudManager
    {
        public static List<OTexture> textures;

        public static CloudCollection backClouds;
        public static CloudCollection foreClouds;

        private static float particleTimer;
        
        public static void init()
        {
            textures = new List<OTexture>();
            textures.Add(Renderer.requireTexture("environment/cloud1"));
            textures.Add(Renderer.requireTexture("environment/wideCloud0"));
            textures.Add(Renderer.requireTexture("environment/wideCloud1"));
            textures.Add(Renderer.requireTexture("environment/wideCloud2"));

            backClouds = new CloudCollection(300, new Random(1));
            foreClouds = new CloudCollection(20, new Random(2));
            foreClouds.speedFactor = 2;

            Timing.setTime(0.01f);
            for (int i = 0; i < 1000; i++)
            {
                particleTimer = 0; // Stop those particles form being spawned, as the particle system won#t move them, yet
                update();
            }
        }

        public static void update()
        {
            backClouds.update();
            foreClouds.update();
            
            while (particleTimer > 1)
            {
                Vector2 shipPosition = ShipManager.playerShip.position;
                float position = backClouds.rnd.NextFloat(-17, 17);
                float speed =  backClouds.rnd.Next(16, 19);

                Vector3 color = new Vector3(backClouds.rnd.NextFloat(0.8f, 1f), backClouds.rnd.NextFloat(0.3f, 0.7f),
                                            backClouds.rnd.NextFloat(0.1f, 0.2f));
                
                ParticleSystem.particles.Add(new Particle(new Color(color),
                                                          new Vector2(-15, position) + shipPosition,
                                                          new Vector2(speed, 0),
                                                          new Vector2(0.3f, 0.3f),
                                                          10));
                ParticleSystem.particles.Add(new Particle(new Color(color),
                                                          new Vector2(-15, position) + shipPosition + new Vector2(0.15f, 0.15f),
                                                          new Vector2(speed, 0),
                                                          new Vector2(0.6f, 0.6f),
                                                          10){transparency = 0.5f});
                
                particleTimer--;
            }
            if (Renderer.bgTimer > Renderer.bluetime + Renderer.transtime)
            {
                particleTimer += Timing.elapsedTime * 30;
            } else
            {
                particleTimer += Timing.elapsedTime * 0;
            }
        }



        public static void draw(int layer)
        {
            if (layer == 0)
                backClouds.draw();
            else if (layer == 1)
                foreClouds.draw();
            else
                throw new Exception("Invalid layer!");
        }
    }
}