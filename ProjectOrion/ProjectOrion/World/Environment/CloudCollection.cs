﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using ProjectOrion.World.Ships;

namespace ProjectOrion.World.Environment
{
    public class CloudCollection
    {
        public List<Cloud> clouds;

        public float timer;
        public float amount;

        public float clearTimer;

        public Random rnd;

        public float speedFactor;

        public CloudCollection(float amount, Random rnd)
        {
            speedFactor = 1;
            this.clouds = new List<Cloud>();
            this.amount = amount;
            this.rnd = rnd;
        }

        public void update()
        {
            timer += Timing.elapsedTime * amount;
            while (timer > 1)
            {
                Vector2 shipPosition = ShipManager.playerShip.position;
                float position = rnd.NextFloat(-17, 17);
                float speed = rnd.Next(9, 13);
                clouds.Add(new Cloud(new Vector2(-15, position) + shipPosition, speedFactor * new Vector2(speed, 0), rnd.Next(1,3), rnd.NextFloat(0, 6.28f), rnd.NextFloat(-1f, 1f)));
                
                timer--;
            }
            foreach (var c in clouds)
                c.update();

            clearTimer += Timing.elapsedTime;
            if (clearTimer > 1)
            {
                clearCloudCollection();
                clearTimer -= 1;
            }
        }
        

        private void clearCloudCollection()
        {
            Vector2 shipPosition = ShipManager.playerShip.position;
            for (int i = clouds.Count - 1; i >= 0; i--)
            {
                if (clouds[i].position.X > shipPosition.X + 15)
                    clouds.RemoveAt(i);
            }
        }

        public void draw()
        {
            foreach (var cloud in clouds)
                cloud.draw();
        }
    }
}