﻿using Microsoft.Xna.Framework;
using ProjectOrion.Camera;

namespace ProjectOrion.World.Environment
{
    public class Cloud
    {
        public Vector2 position;
        public Vector2 velocity;

        public float rotation;
        public float rotationVelocity;
        
        public int texture;

        public Cloud(Vector2 position, Vector2 velocity, int texture, float rotation, float rotationVelocity)
        {
            this.position = position;
            this.velocity = velocity;
            this.texture = texture;

            this.rotation = rotation;
            this.rotationVelocity = rotationVelocity;
        }

        public void update()
        {
            position += velocity * Timing.elapsedTime;
            rotation += rotationVelocity * Timing.elapsedTime;
        }

        public void draw()
        {
            var tex = CloudManager.textures[this.texture];

            // Awesome color stuff
            Color wolkfarb;
            float bgTimer = Renderer.bgTimer;
            float bluetime = Renderer.bluetime;
            float transtime = Renderer.transtime;

            if (bgTimer < bluetime)
            {
                wolkfarb = new Color(1f, 1f, 1f);
            }
            else if (bgTimer < transtime + bluetime)
            {
                wolkfarb = new Color(
                    1-((bgTimer - bluetime) / transtime) * 0.9f,
                    1-((bgTimer - bluetime) / transtime) * 0.9f,
                    1-((bgTimer - bluetime) / transtime) * 0.9f);
            }
            else
            {
                wolkfarb = new Color(0.1f,0.1f,0.1f);
            }



            Renderer.draw(tex, position, tex.texture.Bounds.Size.ToVector2() / Projector.scale,
                          rotation, new Vector2(0.5f, 0.5f),
                          wolkfarb, 0.5f);
        }
    }
}