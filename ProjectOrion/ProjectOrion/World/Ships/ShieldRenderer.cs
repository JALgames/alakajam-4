﻿using System.Collections.Generic;
using System.Threading;
using Microsoft.Xna.Framework;
using ProjectOrion.Camera;

namespace ProjectOrion.World.Ships
{
    public class ShieldRenderer
    {
        private OTexture texture;

        private List<float> strengths;
        
        public ShieldRenderer()
        {
            texture = Renderer.requireTexture("spaceship/spaceshipshields");
            strengths = new List<float>(8);
            for (int i = 0; i < 8; i++)
                strengths.Add(0);
        }

        public void draw(Vector2 position, float startAngle, float endAngle)
        {
            float minStrength = 0.9f;
            for (int i = 0; i < 8; i++)
            {
                strengths[i] -= Timing.elapsedTime;
                if (strengths[i] < minStrength)
                    strengths[i] = minStrength;
            }

            float actualStart = startAngle + MathHelper.Pi / 4;
            float actualEnd = endAngle - MathHelper.Pi / 4;

            float delta = actualEnd - actualStart;
            float step = delta / 8;
            
//            for (float r = 0; r < MathHelper.TwoPi; r += MathHelper.PiOver4)
            for (int i = 0; i < 8; i++)
                drawShield(position, actualStart + step * i + MathHelper.PiOver2, strengths[i]);
        }
            
        public void drawShield(Vector2 shipPosition, float angle, float strength)
        {
            Vector2 size = texture.texture.Bounds.Size.ToVector2() / Projector.scale;
            
            Renderer.draw(texture, shipPosition, size, angle, new Vector2(0.5f, 0.5f), Color.White, strength);
        }

        public void registerImpact(float angle)
        {
            angle += MathHelper.TwoPi;
            angle += MathHelper.PiOver2;
            angle %= MathHelper.TwoPi; // Just to be sure this is in the [0, two pi) range

            int index = (int) (angle / MathHelper.PiOver4);
            if (index < 0 || index >= 8)
                return;
            strengths[index] = 1;
        }
    }
}