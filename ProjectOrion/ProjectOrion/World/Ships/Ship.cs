﻿using System;
using System.Collections.Generic;
using System.Net;
using Microsoft.Xna.Framework;
using ProjectOrion.Camera;
using ProjectOrion.Geometry;
using ProjectOrion.World.Particles;
using ProjectOrion.World.Player;
using ProjectOrion.World.Ships.Guns;
using ProjectOrion.World.Ships.Interactions;

namespace ProjectOrion.World.Ships
{
    public class Ship
    {
        public InteractionCollection interactions;
        public List<Gun> guns;

        public float damage;
        public float smokeCounter;

        public float fireAmount;
        public float fireCounter;
        
        public float shield = 1;
        public bool shieldActive => shield >= 0.5f;
        public float shieldPos;
        
        
        public Vector2 position;

        public OTexture texture;

        public OTexture solidWhite;

        // Animations
        public OTexture[] power = new OTexture[10]; // shield engine lightning sparks
        public OTexture[] engine0 = new OTexture[2]; // engine power small wheel
        public OTexture[] engine1 = new OTexture[2]; // engine power big wheel
        public OTexture[] engine2 = new OTexture[3]; // engine power gear
        public OTexture[] portrait = new OTexture[9]; // Portraits (0-2: P1; 3-5: P2; 6-9: P3) (Left, Center, Right)

        // Ships systems
        public int shields = 1;
        public OTexture[] shieldTowers = new OTexture[2];
        public int engine = 1;
        public float enginespeed = 0.1f;
        public OTexture[] brokenRope = new OTexture[2];


        public OTexture[] ludicrousSpeed = new OTexture[2];


        // Shield Power animation phases
        public int sp_animationPhase = 0;
        public int sp_animationPhase2 = 3;
        //public int animationPhase3 = 8;

        // Engine Power animation phases
        public int ep_animationPhase0 = 0; // big wheel
        public int ep_animationPhase1 = 0; // small wheel
        public int ep_animationPhase2 = 0; // gear


        public float sp_timeUntilNextFrame = 0; // shield power
        public float ep_timeUntilNextFrame0 = 0; // engine Power
        public float ep_timeUntilNextFrame1 = 0; // engine Power
        public float ep_timeUntilNextFrame2 = 0; // engine Power

        public bool boost;
        public float boostStrength;
        public float boostTime;

        public ShieldRenderer shieldRenderer;
        
        public List<AxisAlignedRectangle> colliders;
        public List<AxisAlignedRectangle> ladders;
        
        public Ship()
        {
            texture = Renderer.requireTexture("spaceship/spaceshipOrion");
            solidWhite = Renderer.requireTexture("%white%");
            for (int i = 1; i <= power.Length; i++)
                power[i - 1] = Renderer.requireTexture("animation/shield-power/power" + i);
            
            for (int i = 0; i < engine0.Length; i++)
                engine0[i] = Renderer.requireTexture("animation/engine-power/bwheel" + i);
            
            for (int i = 0; i < engine1.Length; i++)
                engine1[i] = Renderer.requireTexture("animation/engine-power/swheel" + i);
            
            for (int i = 0; i < engine2.Length; i++)
                engine2[i] = Renderer.requireTexture("animation/engine-power/gear" + i);
            
            for (int i = 0; i < portrait.Length; i++)
                portrait[i] = Renderer.requireTexture("animation/portraits/p" + i);

            for (int i = 0; i < shieldTowers.Length; i++)
                shieldTowers[i] = Renderer.requireTexture("spaceship/powerMasts" + i);

            for (int i = 0; i < brokenRope.Length; i++)
                brokenRope[i] = Renderer.requireTexture("animation/engine-power/badGear" + i);
            
            for (int i = 0; i < ludicrousSpeed.Length; i++)
                ludicrousSpeed[i] = Renderer.requireTexture("spaceship/ludicrousSpeed" + i);

            position = new Vector2(0, 0);

            colliders = new List<AxisAlignedRectangle>();
            colliders.Add(new AxisAlignedRectangle(-3, 1.63f, 5.8f, 0.1f)); // Bottom floor
            colliders.Add(new AxisAlignedRectangle(-2.05f, 0.5f, 0.05f, 1)); // Bottom left end
            
            colliders.Add(new AxisAlignedRectangle(-2.4f, 0.439f, 4.2f, 0.125f)); // Middle floor
            colliders.Add(new AxisAlignedRectangle(-2.4f, -0.5f, 0.1f, 1)); // Middle left end
            
            colliders.Add(new AxisAlignedRectangle(-2.4f, -0.69f, 4.1f, 0.125f)); // Top floor
            colliders.Add(new AxisAlignedRectangle(-0.5f, -1.7f, 0.1f, 1.2f)); // Top left end
            
            colliders.Add(new AxisAlignedRectangle(2.8f, -1.5f, 0.1f, 3.5f)); // Right end

            ladders = new List<AxisAlignedRectangle>();
            ladders.Add(new AxisAlignedRectangle(2.1f, -0.5f, 0.5f, 2f));
            
            shieldRenderer = new ShieldRenderer();
            
            interactions = new InteractionCollection();
            guns = new List<Gun>();
            Gun gunTop = new Gun(new Vector2(0, -1.85f), -MathHelper.PiOver2);
            guns.Add(gunTop);
            Gun gunBottom = new Gun(new Vector2(0, 1.7f), MathHelper.PiOver2);
            guns.Add(gunBottom);
            
            interactions.interactions.Add(new SteeringWheel(new Vector2(0, -1.2f)));
            interactions.interactions.Add(new ShootInteraction(new Vector2(-1.5f, -0.05f), guns));
            interactions.interactions.Add(new ShieldPositioningInteraction(new Vector2(1.1f, 1.1f)));
        }

        public virtual void update()
        {
            float targetBoost = boost ? 1 : 0;
            int d = Math.Sign(targetBoost - boostStrength);
            boostStrength += 3 * d * Timing.elapsedTime;
            if (d != Math.Sign(targetBoost - boostStrength))
                boostStrength = targetBoost;
            boostTime += Timing.elapsedTime;
            
            if (damage > 0.2f)//Input.startedPress(Microsoft.Xna.Framework.Input.Keys.F))
            {
                engine = 0;
                shields = 0;
            }
//            if (Input.startedPress(Microsoft.Xna.Framework.Input.Keys.G))
//            {
//                engine = 1;
//                shields = 1;
//            }

            shield += 0.15f * Timing.elapsedTime;
            shield = MathHelper.Clamp(shield, 0, 1);
            
            interactions.offset = position;
            
            foreach (var gun in guns)
                gun.update();
            
        }

        public virtual void draw()
        {
            Vector2 position = new Vector2((int)(this.position.X * 16) / 16f, (int)(this.position.Y * 16) / 16f);
            
            Vector2 size = texture.texture.Bounds.Size.ToVector2() / Projector.scale;
            Renderer.draw(texture, position - size / 2, size);

            float playerPosX = PlayerManager.players[0].position.X - position.X;
            // Watching portraits
            if (playerPosX < -1)
            {
                Renderer.draw(portrait[0], position - size / 2, size);
                
            }
            else if (playerPosX > 0.5)
            {
                Renderer.draw(portrait[2], position - size / 2, size);
            }

            if (playerPosX < 0)
            {
                Renderer.draw(portrait[3], position - size / 2, size);
            }
            else if (playerPosX > 1.5)
            {
                Renderer.draw(portrait[5], position - size / 2, size);
            }

            if (playerPosX < 1)
            {
                Renderer.draw(portrait[6], position - size / 2, size);
            }
            else if (playerPosX > 2)
            {
                Renderer.draw(portrait[8], position - size / 2, size);
            }

            int coverCount = 3;
            if (this == ShipManager.playerShip)
                coverCount = 3 - ShipManager.playerLives;
            if (coverCount >= 1)
                Renderer.draw(solidWhite, position + new Vector2(0 / 16f, -9 / 16f), new Vector2(7 / 16f, 12 / 16f), new Color(143, 86, 59));
            if (coverCount >= 3)
                Renderer.draw(solidWhite, position + new Vector2(11 / 16f, -9 / 16f), new Vector2(7 / 16f, 12 / 16f), new Color(118, 66, 138));
            if (coverCount >= 2)
                Renderer.draw(solidWhite, position + new Vector2(22 / 16f, -9 / 16f), new Vector2(7 / 16f, 12 / 16f), new Color(143, 151, 74));

            // (Damaged) Systems
            if (shields != 0)
            {
                Renderer.draw(shieldTowers[0], position - size / 2, size);
            }
            else
            {
                Renderer.draw(shieldTowers[1], position - size / 2, size);
            }
            // Timed animations
            if (sp_animationPhase > 14)
            {
                sp_animationPhase = 0;
            }
            if (sp_animationPhase2 > 14)
            {
                sp_animationPhase2 = 0;
            }
            if (ep_animationPhase0 > 1)
            {
                ep_animationPhase0 = 0;
            }
            if (ep_animationPhase1 > 1)
            {
                ep_animationPhase1 = 0;
            }
            if (ep_animationPhase2 > 2)
            {
                ep_animationPhase2 = 0;
            }
            //if (animationPhase3 > 12)
            //{
            //    animationPhase3 = 0;
            //}
            if (shields > 0)
            {
                if (sp_animationPhase < 10)
                    Renderer.draw(power[sp_animationPhase], position - size / 2, size);
                if (sp_animationPhase2 < 10)
                    Renderer.draw(power[sp_animationPhase2], position - size / 2, size);
            }
            if (engine >= 0)
            {
                Renderer.draw(engine0[ep_animationPhase0], position - size / 2, size);
            } else
            {
                Renderer.draw(engine0[0], position - size / 2, size);
            }
            Renderer.draw(engine1[ep_animationPhase1], position - size / 2, size);

            if (engine > 0)
            {
                Renderer.draw(engine2[ep_animationPhase2], position - size / 2, size);
            }
            else if (engine == -1)
            {
                Renderer.draw(brokenRope[1], position - size / 2, size);
            }
            else if (engine == 0)
            {
                Renderer.draw(brokenRope[0], position - size / 2, size);
                if (ep_animationPhase2 == 0)
                {
                    engine = -1;
                }
            }
            //if (animationPhase3 < 10)
            //    Renderer.draw(power[animationPhase3], position - size / 2, size);
            if (sp_timeUntilNextFrame < 0)
            {
                sp_animationPhase++;
                sp_animationPhase2++;
                //animationPhase3++;
                sp_timeUntilNextFrame += 0.06f;
            }
            if (ep_timeUntilNextFrame0 < 0)
            {
                ep_animationPhase0++;
                ep_timeUntilNextFrame0 += 0.1f;
            }
            if (ep_timeUntilNextFrame1 < 0)
            {
                ep_animationPhase1++;
                ep_timeUntilNextFrame1 += 0.08f;
            }
            if (ep_timeUntilNextFrame2 < 0)
            {
                ep_animationPhase2++;
                ep_timeUntilNextFrame2 += enginespeed;
            }
            //Renderer.draw(power[animationPhase], position - size / 2, size);

            //for (int i = 0; i < power.Length; i++)
            //{
            //   Renderer.draw(power[i], position - size / 2, size);
            //}
            sp_timeUntilNextFrame -= Timing.elapsedTime;
            ep_timeUntilNextFrame0 -= Timing.elapsedTime;
            ep_timeUntilNextFrame1 -= Timing.elapsedTime;
            ep_timeUntilNextFrame2 -= Timing.elapsedTime;


            for (int i = 0; i < 2; i++)
            {
                float strength = (float)Math.Sin(boostTime * 32 + i * MathHelper.Pi);
                if (i == 0)
                    strength = 1;

                strength *= boostStrength;
                
                double boostWobble = Math.Sin(boostTime * 12) * 0.05f + Math.Sin(boostTime * 13) * 0.03f +
                                     Math.Sin(boostTime * 17) * 0.02f;
                Vector2 boostSize = ludicrousSpeed[i].size / Projector.scale;
                Renderer.draw(ludicrousSpeed[i], position - boostSize / 2 + new Vector2(-0.8f + (float)boostWobble, 0), boostSize, strength);
            }
            
            foreach (var gun in guns)
                gun.draw(position);
            
            if (shieldActive)
                shieldRenderer.draw(position, shieldPos - MathHelper.Pi * 0.5f, shieldPos + MathHelper.Pi * 0.5f);

            if (damage >= 1)
            {
                fireAmount += Timing.elapsedTime * 0.2f;
            }

            fireCounter += fireAmount * Timing.elapsedTime * 50;
            while (fireCounter > 1)
            {
                float angle = rnd.NextFloat(0, 6.28f);

                Vector2 particlePosition = this.position + new Vector2(-1.2f, 1.2f);
                Vector2 d = new Vector2((float) Math.Cos(angle), (float) Math.Sin(angle));
                d *= new Vector2(1, 1.3f);

                float particleSize = rnd.NextFloat(4 / 16f, 8 / 16f);
                float lifetime = rnd.NextFloat(0.2f, 1.5f);

                float r = rnd.NextFloat(0.1f, 0.9f);
                float g = rnd.NextFloat(0.05f, r);
                
                ParticleSystem.particles.Add(new Particle(new Color(r, g, rnd.NextFloat(0, 0.1f)), particlePosition, d * 1.5f, new Vector2(particleSize), lifetime)
                {
                    acceleration = new Vector2(1.5f, 0.1f),
                    rotationVelocity =  rnd.NextFloat(-8, 8)
                });
                
                fireCounter--;
            }
            
            smokeCounter += (float)Math.Sqrt(MathHelper.Clamp(damage, 0, 1.5f)) * Timing.elapsedTime * 100; // Clamp to 1.5 instead of 1 because it looks better! (22.4% better, to be exact!)
            while (smokeCounter > 1)
            {
                float angle = rnd.NextFloat(0, 6.28f);

                Vector2 particlePosition = this.position + new Vector2(-1.2f, 1.2f);
                Vector2 d = new Vector2((float) Math.Cos(angle), (float) Math.Sin(angle));
                d *= new Vector2(1, 0.6f);

                float particleSize = rnd.NextFloat(4 / 16f, 8 / 16f);
                float lifetime = rnd.NextFloat(0.2f, 1.5f);

                float color = rnd.NextFloat(0.1f, 0.25f);
                
                ParticleSystem.particles.Add(new Particle(new Color(color, color, color), particlePosition, d * 1.5f, new Vector2(particleSize), lifetime)
                {
                    acceleration = new Vector2(1.5f, 0),
                    rotationVelocity =  rnd.NextFloat(-8, 8)
                });
                
                smokeCounter--;
            }
        }
        Random rnd = new Random(); // A sneaky random won't do any harm here, will it?

        public void hitShield(float damage)
        {
            // Shield has infinite lives
//            shield -= damage;
//            if (!shieldActive)
//                shield = 0;
        }

        public bool shielded(float angle)
        {
            float margin = 0.02f;
            float shieldStart = shieldPos - MathHelper.Pi * (0.5f - margin);
            float shieldEnd = shieldPos + MathHelper.Pi * (0.5f + margin);
            while (angle < shieldStart)
                angle += MathHelper.TwoPi;

            return angle > shieldStart && angle < shieldEnd;
        }

        public void move(Vector2 d)
        {
            if (boost)
                d *= new Vector2(2, 1);
            position += d + new Vector2(0, -0.1f); // By moving the ship up a bit, we ensure that the player does not slide across the floor

            // This really is not working as well as it should be
//            foreach (var ship in ShipManager.ships)
//            {
//                if (ship != this)
//                {
//                    float dist = (ship.position - position).Length();
//                    if (dist < 5.5f)
//                    {
//                        position -= d + new Vector2(0, -0.1f);;
//                        return;
//                    }
//                }
//            }
            
            foreach (var player in PlayerManager.players)
            {
                if (player.collides())
                {
                    player.position += d;
                }
            }
            position -= new Vector2(0, -0.1f);
        }
    }
}