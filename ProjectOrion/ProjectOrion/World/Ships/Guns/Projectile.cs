﻿using System;
using Microsoft.Xna.Framework;
using ProjectOrion.Camera;
using ProjectOrion.Geometry;
using ProjectOrion.World.Particles;

namespace ProjectOrion.World.Ships.Guns
{
    public class Projectile
    {
        public Vector2 position;
        public Vector2 velocity;
        
        public OTexture texture;

        private bool alive;

        public Projectile(Vector2 position, Vector2 velocity)
        {
            alive = true;
            this.position = position;
            this.velocity = velocity;
            texture = Renderer.requireTexture("%white%");
        }
        

        public void update()
        {
            if (!alive)
                return;
            int step = 3;
            float invStep = 1f / step;
            for (int i = 0; i < step; i++)
            {
                Vector2 oldPosition = position;
                position += velocity * Timing.elapsedTime * invStep;

                foreach (var ship in ShipManager.ships)
                {
                    if (!ship.shieldActive)
                        continue;
                    
                    Vector2 d = position - ship.position;
                    float angle = (float) Math.Atan2(d.Y, d.X);
                    
                    if (!ship.shielded(angle))
                        continue;
                    
                    float shieldR = 3.82f;
                    float newD = (position - ship.position).Length();
                    if (newD < shieldR)
                    {
                        float oldD = (oldPosition - ship.position).Length();
                        if (oldD > newD)
                        {
                            alive = false;
                            ship.shieldRenderer.registerImpact(angle);

                            float speed = velocity.Length();
                            
                            ParticleSystem.shieldEffect(position + velocity * 0.2f / speed, angle);

                            ship.hitShield(0.25f);
                        }
                    }
                }

                foreach (var helibuddy in ShipManager.helibuddies) // I'm so sorry for doing this. They did not do anything wrong!
                {
                    Vector2 d = helibuddy.position - position;
                    d *= new Vector2(0.3f, 0.7f);
                    if (d.Length() < 0.5f)
                    {
                        float angle = (float) Math.Atan2(velocity.Y, velocity.X);
                        ParticleSystem.explosion(position, angle + MathHelper.Pi);
                        helibuddy.damage += 0.15f;
                        alive = false;
                        return;
                    }
                }

                Ship hitShip;
                
                bool collision =
                    ShipManager.collides(AxisAlignedRectangle.fromCenterSize(position, new Vector2(0.25f)), out hitShip);
                if (collision)
                {
                    float angle = (float) Math.Atan2(velocity.Y, velocity.X);
                    ParticleSystem.explosion(position, angle + MathHelper.Pi);
                    hitShip.damage += 0.1f;
                    alive = false;
                    
                }
            }
        }

        public void draw()
        {
            if (!alive)
                return;
            Vector2 size = new Vector2(0.25f, 0.25f);
            Renderer.draw(texture, position - size / 2, size, new Color(50, 200, 255));
        }
    }
}