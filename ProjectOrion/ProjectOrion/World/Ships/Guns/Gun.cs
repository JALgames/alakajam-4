﻿using System;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using ProjectOrion.Camera;

namespace ProjectOrion.World.Ships.Guns
{
    public class Gun
    {
        public Vector2 position;
        public float reloadTime;
        public float angle;

        public float minAngle;
        public float maxAngle;
        public float defaultAngle;
        
        public float temperature;

        public OTexture barrelTexture;

        public Gun(Vector2 position, float defaultAngle)
        {
            this.defaultAngle = defaultAngle;
            this.position = position;

            barrelTexture = Renderer.requireTexture("%white%");
            minAngle = MathHelper.Pi * 0.55f;
            maxAngle = MathHelper.Pi * 1.45f;
        }
        
        public void update()
        {
            if (temperature > 0)
            {
                temperature -= 50 * Timing.elapsedTime;
            }
            if (reloadTime > 0)
            {
                reloadTime -= Timing.elapsedTime;
                if (reloadTime < 0)
                    reloadTime = 0;
            }
        }

        public void aim(Vector2 target, Vector2 shipPosition)
        {
            Vector2 d = target - position - shipPosition;
            float angle = (float)Math.Atan2(d.Y, d.X);
            angle += defaultAngle;
            while (angle < 0)
                angle += MathHelper.TwoPi;
            this.angle = MathHelper.Clamp(angle, minAngle, maxAngle) - defaultAngle;
            
        }
        
        public void fire(Vector2 shipPosition)
        {
            if (reloadTime <= 0)
            {
                Vector2 d = new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));

                float centerDistance = position.Length();
                float endDistance = (position + d * 0.1f).Length();
                if (endDistance <= centerDistance)
                    return; // Shots should be fired outwards
                
                float barrelLength = 1;
                
                Projectile p = new Projectile(shipPosition + position + d * barrelLength, d * 15);
                WeaponsManager.projectiles.Add(p);
                reloadTime = 0.25f;
                temperature += 40;
            }
        }

        public void draw(Vector2 shipPosition)
        {
            Vector3 black = new Vector3(0.2f, 0.2f, 0.2f);
            Vector3 red = new Vector3(1f, 0.2f, 0.2f);
            Vector3 yellow = new Vector3(1f, 0.7f, 0.2f);
            Vector3 white = new Vector3(0.95f, 0.95f, 0.95f);

            Vector3 color;
            if (temperature < 300)
            {
                color = black;
            }
            else if (temperature >= 300 && temperature < 600)
            {
                float p = (temperature - 300) / 300;
                color = black * (1 - p) + red * p;
            }
            else if (temperature >= 600 && temperature < 900)
            {
                float p = (temperature - 600) / 300;
                color = red * (1 - p) + yellow * p;
            }
            else if (temperature >= 900 && temperature < 1200)
            {
                float p = (temperature - 900) / 300;
                color = yellow * (1 - p) + white * p;
            }
            else 
            {
                color = white;
            }
            
            Renderer.draw(barrelTexture, shipPosition + position, new Vector2(1f, 0.25f), angle, new Vector2(0, 0.5f), new Color(color));
            Renderer.draw(barrelTexture, shipPosition + position, new Vector2(1f, 0.125f), angle, new Vector2(0, 0), new Color(color - new Vector3(0.1f, 0.1f, 0.1f)));
            float glowStrength = MathHelper.Clamp((temperature - 300) / 1500, 0, 0.3f);
            Renderer.draw(barrelTexture, shipPosition + position, new Vector2(1.1f, 0.45f), angle, new Vector2(0, 0.5f), new Color(color), glowStrength);
        }
    }
}