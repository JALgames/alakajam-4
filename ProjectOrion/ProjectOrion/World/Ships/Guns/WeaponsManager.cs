﻿using System.Collections.Generic;

namespace ProjectOrion.World.Ships.Guns
{
    public static class WeaponsManager
    {
        public static List<Projectile> projectiles;

        public static void init()
        {
            projectiles = new List<Projectile>();
        }

        
        public static void update()
        {
            foreach (var projectile in projectiles)
                projectile.update();
        }
        public static void draw()
        {
            foreach (var projectile in projectiles)
                projectile.draw();
        }
    }
}