﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using ProjectOrion.Geometry;
using ProjectOrion.World.Player;
using ProjectOrion.World.Player.AI;
using ProjectOrion.World.Ships;
using ProjectOrion.World.Ships.Guns;

namespace ProjectOrion.World.Ships
{
    public static class ShipManager
    {
        public static List<Ship> ships;
        public static List<Heli> helibuddies; // Aren't they cute, those little bastards?
        public static Ship playerShip;

        public static int playerLives;

        public static void init()
        {
            playerLives = 3;
            ships = new List<Ship>();
            helibuddies = new List<Heli>();
            
            
            playerShip = new PlayerShip();
            
            ships.Add(playerShip);
            

            WeaponsManager.init(); // Now that I think about it, that should probably be in the world namespace isntead, but oh well...
        }

        public static void update()
        {
            foreach (var ship in ships)
                ship.update();
            foreach (var heli in helibuddies)
                heli.update();

            for (int i = helibuddies.Count - 1; i >= 0; i--)
            {
                if (helibuddies[i].damage > 1)
                {
                    helibuddies.RemoveAt(i);
                }
            }
            WeaponsManager.update();
        }

        public static void draw()
        {
            foreach (var ship in ships)
                ship.draw();
            foreach (var heli in helibuddies)
                heli.draw();
            WeaponsManager.draw();
        }

        public static bool collides(AxisAlignedRectangle collider)
        {
            Ship throwaway;
            return collides(collider, out throwaway);
        }

        public static bool collides(AxisAlignedRectangle collider, out Ship collidingShip)
        {
            foreach (var ship in ships)
                foreach (var worldCol in ship.colliders)
                {
                    if (collider.intersectsAxisAlignedRectangle(worldCol.translateAxisAlignedRectangle(ship.position)))
                    {
                        collidingShip = ship;
                        return true;
                    }
                }

            for (int i = ships.Count - 1; i >= 0; i--)
            {
                if (ships[i].fireAmount >= 1)
                {
                    for (int j = PlayerManager.players.Count - 1; j >= 1; j--)
                    {
                        var enemy = PlayerManager.players[j] as EnemyCharacter;
                        if (enemy.ship == ships[i])
                            PlayerManager.killEnemy(enemy);
                    }
                    ships.RemoveAt(i);
                }
            }

            collidingShip = null;
            return false;
        }

        public static bool ladders(AxisAlignedRectangle collider)
        {
            foreach (var ship in ships)
                foreach (var ladder in ship.ladders)
                    if (collider.intersectsAxisAlignedRectangle(ladder.translateAxisAlignedRectangle(ship.position)))
                        return true;

            return false;
        }
    }
}