﻿using System;
using Microsoft.Xna.Framework;
using ProjectOrion.Camera;
using ProjectOrion.World.Player;

namespace ProjectOrion.World.Ships
{
    public class Enemy : Ship
    {
        public Enemy()
        {
            position = new Vector2(6.2f, 0);
            PlayerManager.players.Add(new EnemyCharacter(this, 1));
        }

        public override void update()
        {
            base.update();
        }
    }
}