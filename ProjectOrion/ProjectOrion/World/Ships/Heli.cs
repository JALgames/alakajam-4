﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Microsoft.Xna.Framework;
using ProjectOrion.Camera;
using ProjectOrion.Geometry;
using ProjectOrion.World.Particles;
using ProjectOrion.World.Player;
using ProjectOrion.World.Ships.Guns;
using ProjectOrion.World.Ships.Interactions;

namespace ProjectOrion.World.Ships
{
    public class Heli
    {
        public OTexture[] body = new OTexture[4];
        public List<Gun> guns;
        public int rotorPhase = 0;
        public float rotorTimer;
        public Vector2 position;

        private Vector2 targetPosition;
        private float shootTimer;

        public float particleTimer;
        public float damage;
        
        Random rnd = new Random();
        
        public Heli(Vector2 position, Vector2 targetPosition)
        {
            this.targetPosition = targetPosition;
            
            damage = 0;
            this.position = position;
            
            body[0] = Renderer.requireTexture("enemies/helibuddybody");
            body[1] = Renderer.requireTexture("enemies/helibuddybody_rotor");
            body[2] = Renderer.requireTexture("enemies/helibuddybody_heck0");
            body[3] = Renderer.requireTexture("enemies/helibuddybody_heck1");

            guns = new List<Gun>();
            Gun sideGun = new Gun(new Vector2(1, 0.5f), -MathHelper.PiOver2);
            sideGun.minAngle = -10000;
            sideGun.maxAngle = 10000;
            guns.Add(sideGun);

        }
        public virtual void update()
        {
            Vector2 actualTarget = targetPosition + ShipManager.playerShip.position;
            Vector2 d = actualTarget - position;
            if (d.Length() > 1.5f)
            {
                position += d * Timing.elapsedTime * 0.3f;
                shootTimer = 0;
            }
            else
            {
                shootTimer += Timing.elapsedTime;
                foreach (var gun in guns)
                {
                    gun.aim(PlayerManager.players[0].position, position);
                    float change = 0.3f * (float) Math.Sin(shootTimer * 3);
                    gun.angle += change;
                }
                if (shootTimer > 5)
                {
                    foreach (var gun in guns)
                        gun.fire(position);
                }
            }
            
            foreach (var gun in guns)
                gun.update();
        }
        public virtual void draw()
        {
            rotorTimer += Timing.elapsedTime;
            if (rotorTimer > 0.08f)
            {
                rotorPhase++;
                rotorTimer = 0;
                if (rotorPhase > 1)
                    rotorPhase -= 2;
            }
            Vector2 size = body[0].size / Projector.scale;
            Renderer.draw(body[0], position - size / 2, size);

            if (rotorPhase == 0)
            {
                Renderer.draw(body[2], position - size / 2, size);
            } else
            {
                Renderer.draw(body[1], position - size / 2, size);
                Renderer.draw(body[3], position - size / 2, size);
            }
            foreach (var gun in guns)
                gun.draw(position);

            particleTimer += 10 * Timing.elapsedTime * damage;
            while (particleTimer > 1)
            {
                float angle = rnd.NextFloat(3f, 3.5f);

                Vector2 particlePosition = this.position + new Vector2(0.3f, -0.25f);
                Vector2 d = new Vector2((float) Math.Cos(angle), (float) Math.Sin(angle));
                d *= new Vector2(1, 0.6f);

                float particleSize = rnd.NextFloat(4 / 16f, 8 / 16f);
                float lifetime = rnd.NextFloat(0.2f, 1.5f);

                float color = rnd.NextFloat(0.1f, 0.25f);
                
                ParticleSystem.particles.Add(new Particle(new Color(color, color, color), particlePosition, d * 3f, new Vector2(particleSize), lifetime)
                {
                    acceleration = new Vector2(1.5f, 0),
                    rotationVelocity =  rnd.NextFloat(-8, 8)
                });
                
                particleTimer--;
            }
            }

    }
}
