﻿using System;
using Microsoft.Xna.Framework;
using ProjectOrion.Camera;
using ProjectOrion.World.Player;

namespace ProjectOrion.World.Ships.Interactions
{
    public class ShieldPositioningInteraction : Interaction
    {
        private Vector2 target;

        private OTexture targetTexture;
        
        public ShieldPositioningInteraction(Vector2 position) : base(position)
        {
            targetTexture = Renderer.requireTexture("%white%");
        }

        public override void update(PlayerCharacter player, Ship ship)
        {
            target = Projector.toWorldPos(Input.mouse.Position);

            Vector2 d = target - ship.position;
            float angle = (float) Math.Atan2(d.Y, d.X);
            ship.shieldPos = angle;

        }

        public override void draw(PlayerCharacter player, Ship ship)
        {
            
            Vector2 size = targetTexture.size / Projector.scale;
            Renderer.draw(targetTexture, target - size / 2, size);
            base.draw(player, ship);
        }
    }
}