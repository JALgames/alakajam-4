﻿using Microsoft.Xna.Framework;
using ProjectOrion.World.Player;

namespace ProjectOrion.World.Ships.Interactions
{
    public abstract class Interaction
    {
        public Vector2 position;

        public Interaction(Vector2 position)
        {
            this.position = position;
        }

        public abstract void update(PlayerCharacter player, Ship ship);

        public virtual void draw(PlayerCharacter player, Ship ship)
        {
            
        }
    }
}