﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using ProjectOrion.World.Player;

namespace ProjectOrion.World.Ships.Interactions
{
    public class SteeringWheel : Interaction
    {
        public SteeringWheel(Vector2 position) : base(position)
        {
        }

        public override void update(PlayerCharacter player, Ship ship)
        {
            ShipManager.playerShip = ship;
            
            float dY = 0;
            if (Input.keyboard.IsKeyDown(Keys.W))
                dY -= 1;
            if (Input.keyboard.IsKeyDown(Keys.S))
                dY += 1;
            
            float dX = 0;
            if (Input.keyboard.IsKeyDown(Keys.A))
                dX -= 1;
            if (Input.keyboard.IsKeyDown(Keys.D))
                dX += 1;
            
            ship.move(new Vector2(0, dY * 3 * Timing.elapsedTime));
            
            ship.move(new Vector2(dX * 3 * Timing.elapsedTime, 0));
        }
    }
}