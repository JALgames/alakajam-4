﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace ProjectOrion.World.Ships.Interactions
{
    public class InteractionCollection
    {
        public List<Interaction> interactions;
        public Vector2 offset;

        public InteractionCollection()
        {
            this.interactions = new List<Interaction>();
            this.offset = new Vector2();
        }

        public Interaction join(Vector2 position)
        {
            Interaction closest = null;
            float distance = float.MaxValue;
            foreach (var inter in interactions)
            {
                float d = ((inter.position + offset - position) * new Vector2(0.6f, 1f)).Length();
                if (d < distance)
                {
                    distance = d;
                    closest = inter;
                }
            }
            if (distance < 0.7f)
                return closest;
            return null;
        }
    }
}