﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using ProjectOrion.Camera;
using ProjectOrion.World.Player;
using ProjectOrion.World.Ships.Guns;

namespace ProjectOrion.World.Ships.Interactions
{
    public class ShootInteraction : Interaction
    {
        private List<Gun> guns;
        private Vector2 target;

        private OTexture targetTexture;
        
        public ShootInteraction(Vector2 position, List<Gun> guns) : base(position)
        {
            this.guns = guns;
            this.target = new Vector2(5, 0);
            targetTexture = Renderer.requireTexture("%white%");
        }

        public override void update(PlayerCharacter player, Ship ship)
        {
            if (Input.mouse.LeftButton == ButtonState.Pressed)
                foreach (var gun in guns)
                    gun.fire(ship.position);

            target = Projector.toWorldPos(Input.mouse.Position);
            foreach (var gun in guns)
                gun.aim(target, ship.position);
        }

        public override void draw(PlayerCharacter player, Ship ship)
        {
            Vector2 size = targetTexture.size / Projector.scale;
            Renderer.draw(targetTexture, target - size / 2, size);
            base.draw(player, ship);
        }
    }
}