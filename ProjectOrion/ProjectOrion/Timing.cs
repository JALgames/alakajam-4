﻿namespace ProjectOrion
{
    public static class Timing
    {
        public static float elapsedTime => actualElapsedTime * speedFactor;
        private static float actualElapsedTime;
        public static float speedFactor = 1;

        public static void setTime(float time)
        {
            actualElapsedTime = time;
        }
    }
}